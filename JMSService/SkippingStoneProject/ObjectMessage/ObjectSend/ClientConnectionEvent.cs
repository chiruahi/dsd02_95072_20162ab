﻿using ObjectMessage.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObjectMessage.ObjectSend
{
    [Serializable]

    public class ClientConnectionEvent
    {
        // id listen for connection
        private string _idListenConnection;

        // state connect
        private bool _stateConnection;

        private ClientConnection _clientConnection;
        
        public ClientConnectionEvent()
        {

        }

        public ClientConnection ClientConnection
        {
            get
            {
                return _clientConnection;
            }

            set
            {
                _clientConnection = value;
            }
        }

        public string IdListenConnection
        {
            get
            {
                return _idListenConnection;
            }

            set
            {
                _idListenConnection = value;
            }
        }

        public bool StateConnection
        {
            get
            {
                return _stateConnection;
            }

            set
            {
                _stateConnection = value;
            }
        }
    }
}
