﻿using SkippingStone.Element;

namespace SkippingStone.Execute
{
    public interface ISkipping
    {
        // Phương thức tính số lần viên đá nảy lên
        int countBounce();

        // Phương thức tính thời gian viên đá nảy
        // Viên đá nảy n lần thì sẽ di chuyển trong (n+1) chu kỳ parabol
        double timeBounce();

        // Phương thức tính toạ độ điểm rơi lần thứ n
        // Viên đá thia lia n lần thì sẽ chạm nước (n+1) lần, tính cả lần cuối không thể nảy lên
        Coordinate getCoordinateBounce(int n);

        // Phương thức tính vận tốc viên đá theo thời gian
        Coordinate getVelocityByTime(double time);

        // Phương thức tính toạ độ viên đá theo thời gian
        Coordinate getCoordinateByTime(double time);

        // Phương thức check va chạm với mặt nước
        // Cần gọi phương thức này trước khi gọi hàm tính vận tốc hay tính toạ độ
        bool getCheckBounce(double time);

        // Phương thức tính vận tốc sau mỗi lần chạm nước thứ n
        Coordinate getVelocityAfterBounce(int n);
    }
}
