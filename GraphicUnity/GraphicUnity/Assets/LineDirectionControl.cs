﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineDirectionControl : MonoBehaviour {
	private LineRenderer lineRenderer;
	private float counter;
	private float dist;
	private const float MAX_DISTANCE_X = 200f;
	private const float MAX_DISTANCE_Y = 200f;
	private const float MAX_DISTANCE_Z = 200f;
	private const float MAX_DISTANCE = 200f;

	public GameObject stone;
	public static Vector3 directionVector;
	private float pIDiv180;
	public Transform origin;
	public Transform destination;
	private Vector3 maxPoint;
	public float lineDrawSpeed = 6f;
	// Use this for initialization
	private Vector3 MaxPoint(){
		return new Vector3 (origin.position.x + directionVector.x,
			origin.position.y + directionVector.y, origin.position.z + directionVector.z);
		} 
	void Start () {
		lineRenderer = GetComponent<LineRenderer> ();
		lineRenderer.SetPosition (0, origin.position);
		lineRenderer.SetWidth (0.45f, 0.45f);
		directionVector = new Vector3 (45, 45, 45);
		maxPoint = MaxPoint ();
		dist = Vector3.Distance (origin.position, maxPoint);
		pIDiv180 = Mathf.PI/180;;
	}
	
	// Update is called once per frame
	void Update () {
		//if (counter < dist) {
			//counter += 0.1f / lineDrawSpeed;
			//float x = Mathf.Lerp (0, dist, counter);
			//Vector3 pointA = origin.position;
			//Vector3 pointB = maxPoint;
			//Vector3 pointALongLine = x * Vector3.Normalize (pointB - pointA) + pointA;
		if (Input.GetKey(KeyCode.A))
		{
			directionVector.x += 1;
		}
		if (Input.GetKey(KeyCode.Q))
		{
			directionVector.x -= 1;
		}
		if (Input.GetKey(KeyCode.W))
		{
			directionVector.z += 1;
		}
		if (Input.GetKey(KeyCode.S))
		{
			directionVector.z -= 1;
		}
		if (Input.GetKey(KeyCode.D))
		{
			directionVector.y += 1;
		}
		if (Input.GetKey(KeyCode.C))
		{
			directionVector.y -= 1;
		}
		lineRenderer.SetPosition (0, origin.position);
		maxPoint = MaxPoint ();
		lineRenderer.SetPosition (1, maxPoint);

		//}
	}

	private float Velocity = 10;
	public void OnSkippingClick(){
		stone.GetComponent<Rigidbody> ().isKinematic = false;
		stone.GetComponent<Rigidbody> ().velocity = new Vector3 (Mathf.Cos (directionVector.x) * Velocity,
			Mathf.Cos (3.14f * directionVector.y / 180) * Velocity, Mathf.Cos (3.14f * directionVector.z / 180) * Velocity);
	}
}
