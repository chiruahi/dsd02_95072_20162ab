﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.Sources;

/// <summary>
/// Save configuration.
/// This class aims to save all the configuration of the softwawre.
/// </summary>
public class SaveConfiguration : MonoBehaviour {
	public InputField ServerIP;
	public InputField Port;
	public InputField MomIp;
	public void StartSaving(){
		if (Config.CurrentDevice == DEVICE_TYPE.CLIENT) {
			PlayerPrefs.SetString ("userName", ServerIP.text);
			PlayerPrefs.SetString ("password", Port.text);
			PlayerPrefs.SetString ("momip", MomIp.text);
			Debug.Log ("Saving configuration");
		}
	}
}
