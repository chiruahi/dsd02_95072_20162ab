﻿using ObjectMessage.ObjectEvent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObjectMessage.ObjectSend
{
    [Serializable]
    class WaveEvent
    {
        private Wave _wave;

        public WaveEvent()
        {
            _wave = new Wave();
        }

        public Wave Wave
        {
            get
            {
                return _wave;
            }

            set
            {
                _wave = value;
            }
        }
    }
}
