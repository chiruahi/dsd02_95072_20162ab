﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObjectMessage.ObjectEvent
{
    [Serializable]
    public class Wave
    {
        private Coordinate _coordinate;

        public Wave()
        {
        }

        public Coordinate Coordinate
        {
            get
            {
                return _coordinate;
            }

            set
            {
                _coordinate = value;
            }
        }
    }
}
