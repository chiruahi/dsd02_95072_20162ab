﻿using ObjectMessage;
using SkippingStoneService;
using SkippingStone.Execute;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using System.Collections;

using UnityEngine.UI;
using System.Collections.Generic;
using Assets.Sources;
using SkippingStone;

public class Client : MonoBehaviour
{
	#region Declare Client Parameter
	public Camera cameraClient1View;
	public Camera cameraClient2View;
	public Camera cameraClient3View;
	public Camera managerView;
	public GameObject skippingWrapper;
	private SkippingAction skippingAction;
	public GameObject clientPanel;
	public GameObject serverPanel;
    private int _distance_to_top_result_form = 10;
	private string currentUserName;
	private string currentPassword;
    // device name
    private string _deviceName;
    private SendData _transmitData;
	private string thisToken;
    private string _username;
	public static Dictionary<GameObject, Skipping> stoneList;
    // id listen init for connection
    private string _clientID;
	public string ClientID { get { return _clientID; } set { _clientID = value; } }
    #endregion



	public void Start()
	{
		try {
			if (!string.IsNullOrEmpty (PlayerPrefs.GetString ("momip"))){
				var abc = PlayerPrefs.GetString ("momip");
				SkippingStoneService.SystemParameters.DOMAIN = PlayerPrefs.GetString ("momip");
			}
		} catch(Exception e){
			Debug.Log ("Lỗi momip");
		}
		//serverPanel.SetActive (true);
		//Config.CurrentDevice = DEVICE_TYPE.CLIENT;
		if (Config.CurrentDevice == DEVICE_TYPE.CLIENT){
	        _transmitData = new SendData();
	        _clientID = getRamdomStringID();
			Init ();
			stoneList = new Dictionary<GameObject, Skipping> ();
		}
    }

	public Role GetRole(string userName){
		if (userName == "client1")
			return Role.CLIENT1;
		if (userName == "client2")
			return Role.CLIENT2;
		if (userName == "client3")
			return Role.CLIENT3;
		return Role.USER;
	}

    #region random string
    private static System.Random random = new System.Random((int)DateTime.Now.Ticks);

    private string getRamdomStringID()
    {
        StringBuilder builder = new StringBuilder();
        char ch;
        for (int i = 0; i < 16; i++)
        {
            ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
            builder.Append(ch);
        }

        return builder.ToString();
    }
    #endregion

	public void Init(){
		clientPanel.SetActive (true);
		serverPanel.SetActive (false);
		if (string.IsNullOrEmpty (PlayerPrefs.GetString ("userName")) || string.IsNullOrEmpty ("password"))
            {
				Debug.Log("Không được thiếu tên tài khoản hoặc pasword");
            }
            else
            {
                ClientConnectionEvent clientEvent = new ClientConnectionEvent();
                ClientConnection clientConnection = new ClientConnection();
				clientConnection.Username = PlayerPrefs.GetString ("userName");
				clientConnection.Password = PlayerPrefs.GetString ("password");
				currentUserName = clientConnection.Username;
				currentPassword = clientConnection.Password;
                clientEvent.ClientConnection = clientConnection;
                clientEvent.IdListenConnection = _clientID;
				clientEvent.StateConnection = true;
				
                // send username and password to MOM
                _transmitData.sendDataInit(clientEvent);

                // listen from server
                MyQueueSubscriber queueSubscriber = new MyQueueSubscriber(_clientID);
                queueSubscriber.OnMessageReceived += new QMessageReceivedDelegate(QueueSubscriber_OnMessageReceived);
		}
	}

    private void QueueSubscriber_OnMessageReceived(System.Object objectMessage)
    {
        if (objectMessage.GetType() == typeof(DetermineClientsEvent))
        {
            this.addRecive((DetermineClientsEvent)objectMessage);
        }
        else if (objectMessage.GetType() == typeof(ClientEvent))
        {
			//Xử lý lúc sự kiện client thêm hoặc hủy kết nối xảy ra
            this.listenClientConnect((ClientEvent)objectMessage);
		} 
		else if (objectMessage.GetType() == typeof(StoneEvent))
		{
			// a client online
			// check client is mine
			// check client in list client?
			// update client state if need
			UnityMainThreadDispatcher.Instance().Enqueue(SkippingStone((StoneEvent)objectMessage));
		}
    }

    #region region execute event

    // add client to recive
    private void addRecive(DetermineClientsEvent determineEvent)
    {
		this._deviceName = determineEvent.ClientName;
		thisToken = determineEvent.ThisToken;

		// connect from device to manager
		// trammit state of this connection
		ClientEvent this_client = new ClientEvent();
		DetermineClient determine_this_client = new DetermineClient();
		determine_this_client.StateConnect = true;
		determine_this_client.SubcriberName = determineEvent.ClientName;
		determine_this_client.TokenName = thisToken;
		this_client.Determine_client = determine_this_client;

		// get key to publish for Client to MOM 
		//string key_connect = determineEvent.ThisToken;

		_transmitData.sendData(this_client, thisToken);

        List<DetermineClient> listClient = new List<DetermineClient>();
        listClient = determineEvent.ListDetermineClients;

        DetermineClient determine = new DetermineClient();
        // add event recive to clients
        for (int index = 0; index < listClient.Count; index++)
        {
            determine = listClient[index];
            //createViewClientContact(determine.SubcriberClient, determine.StateConnect);
           // if (determine.StateConnect)
           // {
                // listen if client connect and send data
                // listen using username
               // MyQueueSubscriber queueSubscriber = new MyQueueSubscriber(determine.SubcriberClient);
			MyQueueSubscriber queueSubscriber = new MyQueueSubscriber(determine.TokenName);
                queueSubscriber.OnMessageReceived += new QMessageReceivedDelegate(QueueSubscriber_OnMessageReceived);
            //}
		}
		Config.CurrentRole = determineEvent.Role;
		UnityMainThreadDispatcher.Instance().Enqueue(ConfigCamera());

    }

    #endregion

    #region listening event when a client execute connect to server
    private void listenClientConnect(ClientEvent client_event)
    {
		DetermineClient client_connection = new DetermineClient();
		client_connection = client_event.Determine_client;

		//this.createViewClientContact(client_connection.TokenName, client_connection.StateConnect);

    }

	public IEnumerator ConfigCamera(){
		if (Config.CurrentRole == Role.MANAGER) 
		{
			cameraClient1View.enabled = false;
			cameraClient2View.enabled = false;
			cameraClient3View.enabled = false;
			managerView.enabled = true;
			clientPanel.SetActive (false);
			serverPanel.SetActive (true);
		}
		else if (Config.CurrentRole == Role.CLIENT1){
			cameraClient1View.enabled = true;
			cameraClient2View.enabled = false;
			cameraClient3View.enabled = false;
			managerView.enabled = false;
			clientPanel.SetActive (true);
			serverPanel.SetActive (false);
		}
		else if (Config.CurrentRole == Role.CLIENT2){
			cameraClient1View.enabled = false;
			cameraClient2View.enabled = true;
			cameraClient3View.enabled = false;
			managerView.enabled = false;
			clientPanel.SetActive (true);
			serverPanel.SetActive (false);
		}
		if (Config.CurrentRole == Role.CLIENT3){
			cameraClient1View.enabled = false;
			cameraClient2View.enabled = false;
			cameraClient3View.enabled = true;
			managerView.enabled = false;
			clientPanel.SetActive (true);
			serverPanel.SetActive (false);
		}
		yield return null;
	}

    private void Disconnect()
    {
        //notification to server and other client
		ClientConnectionEvent clientEvent = new ClientConnectionEvent();
		ClientConnection clientConnection = new ClientConnection();
		clientConnection.Username = currentUserName;
		clientConnection.Password = currentPassword;
		clientEvent.ClientConnection = clientConnection;
		clientEvent.IdListenConnection = _clientID;
		clientEvent.StateConnection = false;

		// send username and password to MOM
		_transmitData.sendDataInit(clientEvent);
    }

	public InputField Velocity;
	public InputField Mass;
	public InputField Inclined;
	public InputField Diameter;
	public InputField Angle;
	public Dropdown Position;
	public Skipping stone;
	public GameObject clientObject;
	public GameObject StoneObject;
	public GameObject stonePrefab;
	private bool IsStarted = false;
	public double TimeSkip = 0.01;
	public int CallTime = 0;
	private const double ZDIAMETER = 6;
	private Skipping skippingMath;

	public IEnumerator SkippingStone(StoneEvent stoneEvent){
		GameObject newStone = Instantiate(stonePrefab);
		newStone.transform.position = new Vector3 (
			(float)stoneEvent.Stone.Coordinate.X_coor, 
			(float)stoneEvent.Stone.Coordinate.Y_coor, 
			(float)stoneEvent.Stone.Coordinate.Z_coor);
		var newSkipping = new Skipping (
			(stoneEvent.Stone.Diameter), 
			(stoneEvent.Stone.Mass),
			(stoneEvent.Stone.Velocity),
			(stoneEvent.Stone.Angle),
			(stoneEvent.Stone.Inclined));
		//newStone.AddComponent<Rigidbody> ();
		/* var initalVelocity = newSkipping.getVelocityAfterBounce (0);
		newStone.GetComponent<Rigidbody> ().velocity = new Vector3(
			(float)initalVelocity.X_coor, 
			(float)initalVelocity.Y_coor, 
			(float)initalVelocity.Z_coor);
			*/
		newStone.GetComponent<Rigidbody> ().mass = (float)stoneEvent.Stone.Mass;
		stoneList.Add (newStone, newSkipping);
		newStone.GetComponent<Rigidbody> ().isKinematic = false;
		var currentVelocityVector = newSkipping.getVelocityAfterBounce (0);
		newStone.GetComponent<Rigidbody> ().velocity = new Vector3 ((float)currentVelocityVector.X_coor, (float)currentVelocityVector.Y_coor, (float)currentVelocityVector.Z_coor);
		TimeCount++;
		ResultString += "So lan nay: " + newSkipping.countBounce() + "\n";
		ResultText.text = ResultString;
		yield return null;
	}

	public void SkippingOnClick(){
		Debug.Log ("SkippingOnClick");

		var stoneEvent = new StoneEvent();
		stoneEvent.Stone = new ObjectMessage.Stone () {
			Diameter = double.Parse (Diameter.text), 
			Mass = double.Parse (Mass.text), 
			Velocity = double.Parse (Velocity.text),
			Angle = double.Parse (Angle.text),
			Inclined = double.Parse (Inclined.text),
			Coordinate = new ObjectMessage.Coordinate(){
				X_coor = StoneObject.transform.position.x, 
				Y_coor = StoneObject.transform.position.y,
				Z_coor = StoneObject.transform.position.z
			}
		};
		var newSkipping = new Skipping (
			double.Parse (Diameter.text), 
			double.Parse (Mass.text), 
			double.Parse (Velocity.text),
			double.Parse (Angle.text),
			double.Parse (Inclined.text));
		_transmitData.sendData (stoneEvent, thisToken);
		if (stoneList == null)
			stoneList = new Dictionary<GameObject, Skipping> ();
		if (stoneList.ContainsKey (StoneObject))
			Debug.Log ("Đá đã được ném");
		else
			stoneList.Add (StoneObject, newSkipping);
		/*skippingMath = new Skipping (stoneEvent.Stone.Diameter, 
			stoneEvent.Stone.Mass, stoneEvent.Stone.Velocity, stoneEvent.Stone.Angle, stoneEvent.Stone.Inclined); 
		*/
		StoneObject.GetComponent<Rigidbody> ().isKinematic = false;
		//StoneObject.GetComponent<Rigidbody> ().velocity = new Vector3 (4, 4, 4);
		var currentVelocityVector = newSkipping.getVelocityAfterBounce (0);
		StoneObject.GetComponent<Rigidbody> ().velocity = new Vector3 ((float)currentVelocityVector.X_coor, (float)currentVelocityVector.Y_coor, (float)currentVelocityVector.Z_coor);
	}

	public Text ResultText;
	private string ResultString;
	private int TimeCount = 0;
    #endregion
}