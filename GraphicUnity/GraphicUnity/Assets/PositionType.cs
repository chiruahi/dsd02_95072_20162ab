﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PositionType {
	SOUTH,
	WEST,
	NORTH,
	EAST
}
