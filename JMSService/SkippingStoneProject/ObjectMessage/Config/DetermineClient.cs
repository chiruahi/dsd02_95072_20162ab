﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObjectMessage.Config
{
    [Serializable]
    public class DetermineClient
    {
        // state disconnect to server
        private bool _stateConnect;

        // list name to listen from other Device
        private string _subcriberName;

        // token this device will public
        private string _tokenName;

        public DetermineClient()
        {
            _stateConnect = false;
            _subcriberName = "";
            TokenName = "";
        }

        public bool StateConnect
        {
            get
            {
                return _stateConnect;
            }

            set
            {
                _stateConnect = value;
            }
        }

        public string SubcriberName
        {
            get
            {
                return _subcriberName;
            }

            set
            {
                _subcriberName = value;
            }
        }

        public string TokenName
        {
            get
            {
                return _tokenName;
            }

            set
            {
                _tokenName = value;
            }
        }
    }
}
