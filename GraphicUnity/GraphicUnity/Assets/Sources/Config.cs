﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SkippingStoneService;
using ObjectMessage;
using Assets.Sources;

namespace Assets.Sources
{
    public class Config
    {
		public static DEVICE_TYPE CurrentDevice = DEVICE_TYPE.MANAGER;
		public static string CurrentName { get; set; }
		public static int CurrentId { get; set; }
		public static Role CurrentRole { get; set; }
		public static Dictionary<Role, int> CameraMap = new Dictionary<Role, int> () { 
			{ Role.MANAGER, 1 },
			{ Role.CLIENT1, 2 },
			{ Role.CLIENT2, 3 },
			{ Role.CLIENT3, 4 },
			{ Role.USER, 5 }
		};
    }

	public enum DEVICE_TYPE{
		CLIENT,
		MANAGER
	}
}
