﻿using Apache.NMS;
using Apache.NMS.Util;
using ObjectMessage.ObjectSend;
using SkippingStoneService.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SkippingStoneService.Connection
{
    public class SendData
    {
        private string _topicName = "SkippingStoneProject";
        private string _activeMQBrokerUrl = SystemParameters.DOMAIN;

        public SendData()
        {
        }

        #region send data

        public void sendData(Object objectInformation, string key_connect)
        {
            IObjectMessage objectMessage;

            IConnectionFactory factory = new NMSConnectionFactory(_activeMQBrokerUrl);
            using (IConnection connection = factory.CreateConnection())
            {
                //Create the Session 
                using (ISession session = connection.CreateSession())
                {
                    IDestination destination = SessionUtil.GetDestination(session, key_connect);

                    IMessageProducer producer = session.CreateProducer(destination);
                    objectMessage = session.CreateObjectMessage(objectInformation);

                    producer.Send(objectMessage);
                }
            }
        }

        // init connection to server
        public void sendDataInit(ClientConnectionEvent objectClientConnection)
        {
            sendData(objectClientConnection, SystemParameters.KEY_REGISTER_CLIENT);
        }
        #endregion
    }
}
