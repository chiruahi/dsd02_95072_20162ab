﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace SkippingStoneService.Data
{
    public class FormatParameters
    {
        /// <summary>
        /// encode string to MD5 to save in SQL server
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static string EncodeStringToMD5(string code)
        {
            MD5 md5 = MD5.Create();
            byte[] inputBytes = System.Text.Encoding.UTF8.GetBytes(code);
            byte[] hashBytes = md5.ComputeHash(inputBytes);

            // Convert the byte array to hexadecimal string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
            {
                sb.Append(hashBytes[i].ToString("X2"));
            }

            return sb.ToString();
        }

        /// <summary>
        /// create a random 32 byte token
        /// </summary>
        /// <returns></returns>
        private static Random random = new Random((int)DateTime.Now.Ticks);

        public static string getRamdomToken()
        {
            StringBuilder builder = new StringBuilder();
            char ch;
            for (int i = 0; i < 8; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }

            return EncodeStringToMD5(builder.ToString());
        }
 
    }
}
