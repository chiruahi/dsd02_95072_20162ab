﻿using System;
using System.Windows.Forms;
using SkippingStone.Element;
using SkippingStone.Execute;

namespace SkippingStone
{
    public partial class Form1 : Form
    {
        private Skipping _skipping;
        private Stone _stone;
        private Coordinate _coor0, _coor;
        private double time;

        public Form1()
        {
            InitializeComponent();
            _skipping = new Skipping();
            _stone = new Stone();
            _coor0 = new Coordinate();
            _coor = new Coordinate();
        }

        private void textBoxDiameter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                _stone.Diameter = Convert.ToDouble(textBoxDiameter.Text);
                //labelResult.Text = " " + _skipping.countBounce(_stone);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex);
                _stone.Diameter = 0;
            }
        }

        private void textBoxMass_TextChanged(object sender, EventArgs e)
        {
            try
            {
                _stone.Mass = Convert.ToDouble(textBoxMass.Text);
                //labelResult.Text = " " + _skipping.countBounce(_stone);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex);
                _stone.Mass = 0;
            }
        }

        private void textBoxVelocity_TextChanged(object sender, EventArgs e)
        {
            try
            {
                _stone.Velocity = Convert.ToDouble(textBoxVelocity.Text);
                //labelResult.Text = " " + _skipping.countBounce(_stone);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex);
                _stone.Velocity = 0;
            }
        }

        private void textBoxAngle_TextChanged(object sender, EventArgs e)
        {
            try
            {
                _stone.Angle = Convert.ToDouble(textBoxAngle.Text);
                //labelResult.Text = " " + _skipping.countBounce(_stone);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex);
                _stone.Angle = 0;
            }
        }

        private void textBoxInclined_TextChanged(object sender, EventArgs e)
        {
            try
            {
                _stone.Inclined = Convert.ToDouble(textBoxInclined.Text);
                //labelResult.Text = " " + _skipping.countBounce(_stone);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex);
                _stone.Inclined = 0;
            }
        }

        private void buttonCount_Click(object sender, EventArgs e)
        {
            //labelResult.Text = "" + _skipping.countBounce(_stone);
            //labelTimeResult.Text = "" + _skipping.timeBounce(_stone);
        }

        private void buttonVeloBounce_Click(object sender, EventArgs e)
        {
            string mes = "";

            // Số lần nảy
            //int n = _skipping.countBounce(_stone) + 1;

            //for (int i = 0; i <= n; i++)
            //{
            //    _coor = _skipping.getVelocityAfterBounce(_stone, i);
            //    mes += "Vận tốc viên đá sau khi chạm nước lần " + i + " : Vx = " + _coor.X_coor + ", Vy = " + _coor.Y_coor + ", Vz = " + _coor.Z_coor + "\n";
            //}

            MessageBox.Show(mes, "Kết quả");
        }

        private void buttonCoorBounce_Click(object sender, EventArgs e)
        {
            string mes = "";

            // Số lần chạm nước = Số lần nảy + 1
            //int n = _skipping.countBounce(_stone) + 1;

            //for (int i = 1; i <= n; i++)
            //{
            //    _coor = _skipping.getCoordinateBounce(_stone, i);
            //    mes += "Toạ độ viên đá chạm nước lần " + i + " : x = " + _coor.X_coor + ", y = " + _coor.Y_coor + ", z = " + _coor.Z_coor + "\n";
            //}
            MessageBox.Show(mes, "Kết quả");
        }

        private void textBoxTime_TextChanged(object sender, EventArgs e)
        {
            try
            {
                time = Convert.ToDouble(textBoxTime.Text);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex);
                time = 0;
            }
        }

        private void buttonCoordinate_Click(object sender, EventArgs e)
        {
            //bool check = _skipping.getCheckBounce(_stone, time);
            //if (check)
              //  labelTime.Text = "Cham";
            //else
              //  labelTime.Text = "Chua cham";

            //_coor = _skipping.getVelocityByTime(_stone, time);
            labelVelocityResult.Text = "Vận tốc:\n Vx = " + _coor.X_coor + ",\n Vy = " + _coor.Y_coor + ",\n Vz = " + _coor.Z_coor;

            //_coor = _skipping.getCoordinateByTime(_stone, time);
            labelCoordinate.Text = "Toạ độ:\n x = " + _coor.X_coor + ",\n y = " + _coor.Y_coor + ",\n z = " + _coor.Z_coor;
        }

        private void textBoxX_TextChanged(object sender, EventArgs e)
        {
            try
            {
                _coor0.X_coor = Convert.ToDouble(textBoxX.Text);
                _skipping = new Skipping(_coor0);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex);
                _coor0.X_coor = 0;
            }
        }

        private void textBoxY_TextChanged(object sender, EventArgs e)
        {
            try
            {
                _coor0.Y_coor = Convert.ToDouble(textBoxY.Text);
                _skipping = new Skipping(_coor0);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex);
                _coor0.Y_coor = 0;
            }
        }

        private void textBoxZ_TextChanged(object sender, EventArgs e)
        {
            try
            {
                _coor0.Z_coor = Convert.ToDouble(textBoxZ.Text);
                _skipping = new Skipping(_coor0);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex);
                _coor0.Z_coor = 0;
            }
        }
    }
}
