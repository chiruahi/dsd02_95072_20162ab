﻿namespace SkippingStone
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelDiameter = new System.Windows.Forms.Label();
            this.labelMass = new System.Windows.Forms.Label();
            this.labelVelocity = new System.Windows.Forms.Label();
            this.labelInclined = new System.Windows.Forms.Label();
            this.labelCount = new System.Windows.Forms.Label();
            this.labelResult = new System.Windows.Forms.Label();
            this.textBoxDiameter = new System.Windows.Forms.TextBox();
            this.textBoxMass = new System.Windows.Forms.TextBox();
            this.textBoxVelocity = new System.Windows.Forms.TextBox();
            this.textBoxInclined = new System.Windows.Forms.TextBox();
            this.buttonCount = new System.Windows.Forms.Button();
            this.labelTime = new System.Windows.Forms.Label();
            this.textBoxTime = new System.Windows.Forms.TextBox();
            this.labelCoordinate = new System.Windows.Forms.Label();
            this.labelAngle = new System.Windows.Forms.Label();
            this.textBoxAngle = new System.Windows.Forms.TextBox();
            this.buttonCoorBounce = new System.Windows.Forms.Button();
            this.labelVelocityResult = new System.Windows.Forms.Label();
            this.buttonCoordinate = new System.Windows.Forms.Button();
            this.labelThiaLia = new System.Windows.Forms.Label();
            this.labelTimeResult = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxX = new System.Windows.Forms.TextBox();
            this.textBoxY = new System.Windows.Forms.TextBox();
            this.textBoxZ = new System.Windows.Forms.TextBox();
            this.buttonVeloBounce = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelDiameter
            // 
            this.labelDiameter.AutoSize = true;
            this.labelDiameter.Location = new System.Drawing.Point(36, 99);
            this.labelDiameter.Name = "labelDiameter";
            this.labelDiameter.Size = new System.Drawing.Size(153, 13);
            this.labelDiameter.TabIndex = 0;
            this.labelDiameter.Text = "Đường kính viên đá (đơn vị m)";
            // 
            // labelMass
            // 
            this.labelMass.AutoSize = true;
            this.labelMass.Location = new System.Drawing.Point(36, 134);
            this.labelMass.Name = "labelMass";
            this.labelMass.Size = new System.Drawing.Size(150, 13);
            this.labelMass.TabIndex = 1;
            this.labelMass.Text = "Khối lượng viên đá (đơn vị kg)";
            // 
            // labelVelocity
            // 
            this.labelVelocity.AutoSize = true;
            this.labelVelocity.Location = new System.Drawing.Point(36, 169);
            this.labelVelocity.Name = "labelVelocity";
            this.labelVelocity.Size = new System.Drawing.Size(147, 13);
            this.labelVelocity.TabIndex = 2;
            this.labelVelocity.Text = "Vận tốc ban đầu (đơn vị m/s)";
            // 
            // labelInclined
            // 
            this.labelInclined.AutoSize = true;
            this.labelInclined.Location = new System.Drawing.Point(36, 237);
            this.labelInclined.Name = "labelInclined";
            this.labelInclined.Size = new System.Drawing.Size(142, 13);
            this.labelInclined.TabIndex = 4;
            this.labelInclined.Text = "Góc lệch gamma (đơn vị độ)";
            // 
            // labelCount
            // 
            this.labelCount.AutoSize = true;
            this.labelCount.Location = new System.Drawing.Point(36, 279);
            this.labelCount.Name = "labelCount";
            this.labelCount.Size = new System.Drawing.Size(113, 13);
            this.labelCount.TabIndex = 5;
            this.labelCount.Text = "Sô lần viên đá nảy lên";
            // 
            // labelResult
            // 
            this.labelResult.AutoSize = true;
            this.labelResult.Location = new System.Drawing.Point(283, 279);
            this.labelResult.Name = "labelResult";
            this.labelResult.Size = new System.Drawing.Size(13, 13);
            this.labelResult.TabIndex = 11;
            this.labelResult.Text = "0";
            // 
            // textBoxDiameter
            // 
            this.textBoxDiameter.Location = new System.Drawing.Point(286, 96);
            this.textBoxDiameter.Name = "textBoxDiameter";
            this.textBoxDiameter.Size = new System.Drawing.Size(100, 20);
            this.textBoxDiameter.TabIndex = 6;
            this.textBoxDiameter.TextChanged += new System.EventHandler(this.textBoxDiameter_TextChanged);
            // 
            // textBoxMass
            // 
            this.textBoxMass.Location = new System.Drawing.Point(286, 131);
            this.textBoxMass.Name = "textBoxMass";
            this.textBoxMass.Size = new System.Drawing.Size(100, 20);
            this.textBoxMass.TabIndex = 7;
            this.textBoxMass.TextChanged += new System.EventHandler(this.textBoxMass_TextChanged);
            // 
            // textBoxVelocity
            // 
            this.textBoxVelocity.Location = new System.Drawing.Point(286, 166);
            this.textBoxVelocity.Name = "textBoxVelocity";
            this.textBoxVelocity.Size = new System.Drawing.Size(100, 20);
            this.textBoxVelocity.TabIndex = 8;
            this.textBoxVelocity.TextChanged += new System.EventHandler(this.textBoxVelocity_TextChanged);
            // 
            // textBoxInclined
            // 
            this.textBoxInclined.Location = new System.Drawing.Point(286, 234);
            this.textBoxInclined.Name = "textBoxInclined";
            this.textBoxInclined.Size = new System.Drawing.Size(100, 20);
            this.textBoxInclined.TabIndex = 10;
            this.textBoxInclined.TextChanged += new System.EventHandler(this.textBoxInclined_TextChanged);
            // 
            // buttonCount
            // 
            this.buttonCount.Location = new System.Drawing.Point(39, 355);
            this.buttonCount.Name = "buttonCount";
            this.buttonCount.Size = new System.Drawing.Size(54, 23);
            this.buttonCount.TabIndex = 12;
            this.buttonCount.Text = "Tính";
            this.buttonCount.UseVisualStyleBackColor = true;
            this.buttonCount.Click += new System.EventHandler(this.buttonCount_Click);
            // 
            // labelTime
            // 
            this.labelTime.AutoSize = true;
            this.labelTime.Location = new System.Drawing.Point(467, 28);
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new System.Drawing.Size(51, 13);
            this.labelTime.TabIndex = 13;
            this.labelTime.Text = "Thời gian";
            // 
            // textBoxTime
            // 
            this.textBoxTime.Location = new System.Drawing.Point(562, 25);
            this.textBoxTime.Name = "textBoxTime";
            this.textBoxTime.Size = new System.Drawing.Size(100, 20);
            this.textBoxTime.TabIndex = 14;
            this.textBoxTime.TextChanged += new System.EventHandler(this.textBoxTime_TextChanged);
            // 
            // labelCoordinate
            // 
            this.labelCoordinate.AutoSize = true;
            this.labelCoordinate.Location = new System.Drawing.Point(467, 86);
            this.labelCoordinate.Name = "labelCoordinate";
            this.labelCoordinate.Size = new System.Drawing.Size(81, 13);
            this.labelCoordinate.TabIndex = 15;
            this.labelCoordinate.Text = "Toạ độ viên đá";
            // 
            // labelAngle
            // 
            this.labelAngle.AutoSize = true;
            this.labelAngle.Location = new System.Drawing.Point(36, 202);
            this.labelAngle.Name = "labelAngle";
            this.labelAngle.Size = new System.Drawing.Size(148, 13);
            this.labelAngle.TabIndex = 3;
            this.labelAngle.Text = "Góc ném ban đầu (đơn vị độ)";
            // 
            // textBoxAngle
            // 
            this.textBoxAngle.Location = new System.Drawing.Point(286, 199);
            this.textBoxAngle.Name = "textBoxAngle";
            this.textBoxAngle.Size = new System.Drawing.Size(100, 20);
            this.textBoxAngle.TabIndex = 9;
            this.textBoxAngle.TextChanged += new System.EventHandler(this.textBoxAngle_TextChanged);
            // 
            // buttonCoorBounce
            // 
            this.buttonCoorBounce.Location = new System.Drawing.Point(470, 355);
            this.buttonCoorBounce.Name = "buttonCoorBounce";
            this.buttonCoorBounce.Size = new System.Drawing.Size(116, 23);
            this.buttonCoorBounce.TabIndex = 19;
            this.buttonCoorBounce.Text = "Toạ độ chạm nước";
            this.buttonCoorBounce.UseVisualStyleBackColor = true;
            this.buttonCoorBounce.Click += new System.EventHandler(this.buttonCoorBounce_Click);
            // 
            // labelVelocityResult
            // 
            this.labelVelocityResult.AutoSize = true;
            this.labelVelocityResult.Location = new System.Drawing.Point(467, 155);
            this.labelVelocityResult.Name = "labelVelocityResult";
            this.labelVelocityResult.Size = new System.Drawing.Size(44, 13);
            this.labelVelocityResult.TabIndex = 17;
            this.labelVelocityResult.Text = "Vận tốc";
            // 
            // buttonCoordinate
            // 
            this.buttonCoordinate.Location = new System.Drawing.Point(634, 355);
            this.buttonCoordinate.Name = "buttonCoordinate";
            this.buttonCoordinate.Size = new System.Drawing.Size(109, 23);
            this.buttonCoordinate.TabIndex = 20;
            this.buttonCoordinate.Text = "Tính theo thời gian";
            this.buttonCoordinate.UseVisualStyleBackColor = true;
            this.buttonCoordinate.Click += new System.EventHandler(this.buttonCoordinate_Click);
            // 
            // labelThiaLia
            // 
            this.labelThiaLia.AutoSize = true;
            this.labelThiaLia.Location = new System.Drawing.Point(36, 316);
            this.labelThiaLia.Name = "labelThiaLia";
            this.labelThiaLia.Size = new System.Drawing.Size(154, 13);
            this.labelThiaLia.TabIndex = 21;
            this.labelThiaLia.Text = "Thời gian \"thia lia\" của viên đá";
            // 
            // labelTimeResult
            // 
            this.labelTimeResult.AutoSize = true;
            this.labelTimeResult.Location = new System.Drawing.Point(283, 316);
            this.labelTimeResult.Name = "labelTimeResult";
            this.labelTimeResult.Size = new System.Drawing.Size(13, 13);
            this.labelTimeResult.TabIndex = 22;
            this.labelTimeResult.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(166, 13);
            this.label1.TabIndex = 23;
            this.label1.Text = "Toạ độ ban đầu           ( x, y, z ) =";
            // 
            // textBoxX
            // 
            this.textBoxX.Location = new System.Drawing.Point(286, 25);
            this.textBoxX.Name = "textBoxX";
            this.textBoxX.Size = new System.Drawing.Size(28, 20);
            this.textBoxX.TabIndex = 24;
            this.textBoxX.TextChanged += new System.EventHandler(this.textBoxX_TextChanged);
            // 
            // textBoxY
            // 
            this.textBoxY.Location = new System.Drawing.Point(330, 25);
            this.textBoxY.Name = "textBoxY";
            this.textBoxY.Size = new System.Drawing.Size(27, 20);
            this.textBoxY.TabIndex = 25;
            this.textBoxY.TextChanged += new System.EventHandler(this.textBoxY_TextChanged);
            // 
            // textBoxZ
            // 
            this.textBoxZ.Location = new System.Drawing.Point(375, 25);
            this.textBoxZ.Name = "textBoxZ";
            this.textBoxZ.Size = new System.Drawing.Size(25, 20);
            this.textBoxZ.TabIndex = 26;
            this.textBoxZ.TextChanged += new System.EventHandler(this.textBoxZ_TextChanged);
            // 
            // buttonVeloBounce
            // 
            this.buttonVeloBounce.Location = new System.Drawing.Point(286, 355);
            this.buttonVeloBounce.Name = "buttonVeloBounce";
            this.buttonVeloBounce.Size = new System.Drawing.Size(133, 23);
            this.buttonVeloBounce.TabIndex = 27;
            this.buttonVeloBounce.Text = "Vận tốc sau chạm nước";
            this.buttonVeloBounce.UseVisualStyleBackColor = true;
            this.buttonVeloBounce.Click += new System.EventHandler(this.buttonVeloBounce_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(770, 401);
            this.Controls.Add(this.buttonVeloBounce);
            this.Controls.Add(this.textBoxZ);
            this.Controls.Add(this.textBoxY);
            this.Controls.Add(this.textBoxX);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelTimeResult);
            this.Controls.Add(this.labelThiaLia);
            this.Controls.Add(this.buttonCoordinate);
            this.Controls.Add(this.labelVelocityResult);
            this.Controls.Add(this.buttonCoorBounce);
            this.Controls.Add(this.textBoxAngle);
            this.Controls.Add(this.labelAngle);
            this.Controls.Add(this.labelCoordinate);
            this.Controls.Add(this.textBoxTime);
            this.Controls.Add(this.labelTime);
            this.Controls.Add(this.buttonCount);
            this.Controls.Add(this.textBoxInclined);
            this.Controls.Add(this.textBoxVelocity);
            this.Controls.Add(this.textBoxMass);
            this.Controls.Add(this.textBoxDiameter);
            this.Controls.Add(this.labelResult);
            this.Controls.Add(this.labelCount);
            this.Controls.Add(this.labelInclined);
            this.Controls.Add(this.labelVelocity);
            this.Controls.Add(this.labelMass);
            this.Controls.Add(this.labelDiameter);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelDiameter;
        private System.Windows.Forms.Label labelMass;
        private System.Windows.Forms.Label labelVelocity;
        private System.Windows.Forms.Label labelInclined;
        private System.Windows.Forms.Label labelCount;
        private System.Windows.Forms.Label labelResult;
        private System.Windows.Forms.TextBox textBoxDiameter;
        private System.Windows.Forms.TextBox textBoxMass;
        private System.Windows.Forms.TextBox textBoxVelocity;
        private System.Windows.Forms.TextBox textBoxInclined;
        private System.Windows.Forms.Button buttonCount;
        private System.Windows.Forms.Label labelTime;
        private System.Windows.Forms.TextBox textBoxTime;
        private System.Windows.Forms.Label labelCoordinate;
        private System.Windows.Forms.Label labelAngle;
        private System.Windows.Forms.TextBox textBoxAngle;
        private System.Windows.Forms.Button buttonCoorBounce;
        private System.Windows.Forms.Label labelVelocityResult;
        private System.Windows.Forms.Button buttonCoordinate;
        private System.Windows.Forms.Label labelThiaLia;
        private System.Windows.Forms.Label labelTimeResult;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxX;
        private System.Windows.Forms.TextBox textBoxY;
        private System.Windows.Forms.TextBox textBoxZ;
        private System.Windows.Forms.Button buttonVeloBounce;
    }
}

