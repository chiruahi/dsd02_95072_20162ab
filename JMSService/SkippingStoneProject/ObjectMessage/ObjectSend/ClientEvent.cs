﻿using ObjectMessage.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObjectMessage.ObjectSend
{
    [Serializable]
    public class ClientEvent
    {
        // state of client
        private DetermineClient _determine_client;

        // key token for listen
        private string _tokenListen;

        public ClientEvent()
        {

        }

        public DetermineClient Determine_client
        {
            get
            {
                return _determine_client;
            }

            set
            {
                _determine_client = value;
            }
        }

        public string TokenListen
        {
            get
            {
                return _tokenListen;
            }

            set
            {
                _tokenListen = value;
            }
        }
    }
}
