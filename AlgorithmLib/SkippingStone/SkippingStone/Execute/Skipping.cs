﻿using System;
using SkippingStone.Element;

namespace SkippingStone.Execute
{
    public class Skipping : ISkipping
    {

        #region Các thông số của viên đá

        private Coordinate _coor0, _coor;
        private Stone _stone;

        // Vận tốc theo phương ngang của viên đá (trong mặt phẳng Oxz)
        double v_horizontal;

        // Vận tốc theo phương dọc của viên đá
        double v_vertical;

        // Chu kỳ hiện tại
        private int numberCycle;

        // Thời gian cho một quỹ đạo parabol
        private double cycleTime;

        const float g = 9.80665F;
        const double pi = Math.PI;
        private double muy;
        private double lota;
        private double phi;
        private double gamma;

        #endregion


        public Skipping()
        {
            _coor = new Coordinate();
            _stone = new Stone();
        }

        public Skipping(double diameter, double mass, double velocity, double angle, double inclined)
        {
            _coor = new Coordinate();
            _stone = new Stone(diameter, mass, velocity, angle, inclined);
        }

        public Skipping(double x_coor, double y_coor, double z_coor, double diameter, double mass, double velocity, double angle, double inclined)
        {
            _coor0 = new Coordinate(x_coor, y_coor, z_coor);
            // Có cần thiết không? Chúng ta có thể dùng coor ban đầu của stone mà???
            _stone = new Stone(x_coor, y_coor, z_coor, diameter, mass, velocity, angle, inclined);
        }

        public int countBounce()
        {
            double bounce;
            phi = _stone.Angle * pi / 180;
            muy = (Math.Cos(phi) + Math.Sin(phi)) / (Math.Cos(phi) - Math.Sin(phi));
            lota = 2 * pi * Math.Sqrt((2 * _stone.Mass * Math.Sin(phi)) / (1000 * _stone.Diameter * (Math.Cos(phi) - Math.Sin(phi))));

            bounce = (Math.Pow(_stone.Velocity * Math.Cos(phi), 2)) / (2 * g * muy * lota);

            int k = (int) bounce;
            if (k > 0) return k;
            return 0;
        }

        public double timeBounce()
        {
            cycleTime = (2 * _stone.Velocity * Math.Sin(phi)) / g;
            int n = countBounce() + 1;
            return (n * cycleTime);
        }

        // Phương thức tính vận tốc theo phương ngang trong quỹ đạo parabol thứ n
        private double getVelocityHorizontal(double velocity, int n)
        {
            v_horizontal = velocity * Math.Cos(phi);

            // Nếu n=0, nghĩa là viên đá chưa chạm nước lần nào, nó sẽ thoát vòng for
            for (int i = 0; i < n; i++)
            {
                // Vận tốc sau va chạm
                v_horizontal = Math.Sqrt(Math.Pow(v_horizontal, 2) - (2 * g * muy * lota));
            }

            return v_horizontal;
        }

        public Coordinate getCoordinateBounce(int n)
        {
            _coor.X_coor = 0;
            _coor.Y_coor = 0;
            _coor.Z_coor = 0;
            gamma = _stone.Inclined * pi / 180;

            // Vận tốc theo phương dọc sẽ không đổi sau khi va chạm
            v_vertical = _stone.Velocity * Math.Sin(phi);

            // delta_X là khoảng cách giữa 2 lần chạm nước
            double delta_X;

            for(int i = 0; i < n; i++)
            {
                // Tính vận tốc ở quỹ đạo parabol thứ i
                v_horizontal = getVelocityHorizontal(_stone.Velocity, i);

                // Tính khoảng cách giữa 2 lần chạm nước liên tiếp
                delta_X = 2 * v_horizontal * v_vertical / g;

                _coor.X_coor += delta_X * Math.Cos(gamma);
                _coor.Z_coor += delta_X * Math.Sin(gamma);
            }

            return _coor;
        }

        // Tính xem viên đá đang nằm trong parabol nào
        private int getNumberCycle(double time)
        {
            // Chu kỳ thời gian là đại lượng không đổi
            cycleTime = (2 * _stone.Velocity * Math.Sin(phi)) / g;
            numberCycle = (int)(time / cycleTime);
            return numberCycle;
        }

        public Coordinate getVelocityByTime(double time)
        {
            getNumberCycle(time);
            double remainingTime = time - (numberCycle * cycleTime);

            if (numberCycle > countBounce())
            {
                _coor.X_coor = 0;
                _coor.Y_coor = 0;
                _coor.Z_coor = 0;
                return _coor;
            }

            // Tính vận tốc trong chu kỳ đó
            v_horizontal = getVelocityHorizontal(_stone.Velocity, numberCycle);
            v_vertical = _stone.Velocity * Math.Sin(phi) - (g * remainingTime);
            gamma = _stone.Inclined * pi / 180;

            _coor.X_coor = v_horizontal * Math.Cos(gamma);
            _coor.Y_coor = v_vertical;
            _coor.Z_coor = v_horizontal * Math.Sin(gamma);
            return _coor;
        }

        public Coordinate getCoordinateByTime(double time)
        {
            _coor = getVelocityByTime(time);
            double v_X = _coor.X_coor;
            //double v_Y = _coor.Y_coor;
            double v_Z = _coor.Z_coor;
            double remainingTime = time - (numberCycle * cycleTime);

            if (numberCycle > countBounce())
            {
                _coor.X_coor = 0;
                _coor.Y_coor = 0;
                _coor.Z_coor = 0;
                return _coor;
            }

            // Tính toạ độ trong chu kỳ đó
            gamma = _stone.Inclined * pi / 180;
            _coor = getCoordinateBounce(numberCycle);
            _coor.X_coor += v_X * remainingTime;
            _coor.Y_coor = _stone.Velocity * Math.Sin(phi) * remainingTime - (g * Math.Pow(remainingTime, 2) / 2);
            _coor.Z_coor += v_Z * remainingTime;
            return _coor;
        }
        
        public bool getCheckBounce(double time)
        {
            int n_old = numberCycle;
            int n_new = getNumberCycle(time);
            if (n_new > n_old)
                return true;
            return false;
        }

        public Coordinate getVelocityAfterBounce(int n)
        {
            if (n > countBounce())
            {
                _coor.X_coor = 0;
                _coor.Y_coor = 0;
                _coor.Z_coor = 0;
                return _coor;
            }  

            v_horizontal = getVelocityHorizontal(_stone.Velocity, n);
            v_vertical = _stone.Velocity * Math.Sin(phi);
            gamma = _stone.Inclined * pi / 180;

            _coor.X_coor = v_horizontal * Math.Cos(gamma);
            _coor.Y_coor = v_vertical;
            _coor.Z_coor = v_horizontal * Math.Sin(gamma);

            return _coor;
        }
    }
}
