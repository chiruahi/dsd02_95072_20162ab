﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObjectMessage.ObjectEvent
{
    [Serializable]
    public class Coordinate
    {
        private double x_coor;
        private double y_coor;
        private double z_coor;

        public double X_coor
        {
            get
            {
                return x_coor;
            }

            set
            {
                x_coor = value;
            }
        }

        public double Y_coor
        {
            get
            {
                return y_coor;
            }

            set
            {
                y_coor = value;
            }
        }

        public double Z_coor
        {
            get
            {
                return z_coor;
            }

            set
            {
                z_coor = value;
            }
        }
    }
}
