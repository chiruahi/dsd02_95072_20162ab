﻿using Apache.NMS;
using Apache.NMS.ActiveMQ;
using Apache.NMS.Util;
using ObjectMessage.Config;
using ObjectMessage.ObjectSend;
using SkippingStoneService.Connection;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace ClientApp
{
    public partial class Client : Form
    {
        private int _distance_to_top_result_form = 10;

        // device name
        private string _deviceName;
        private SendData _transmitData;

        private string thisToken;

        private string _username;

        // id listen init for connection
        private string _clientID;


        public Client()
        {
            InitializeComponent();

            btn_disconnect.Enabled = false;

            _transmitData = new SendData();
            _clientID = getRamdomStringID();

        }

        #region random string
        private static Random random = new Random((int)DateTime.Now.Ticks);

        private string getRamdomStringID()
        {
            StringBuilder builder = new StringBuilder();
            char ch;
            for (int i = 0; i < 16; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }

            return builder.ToString();
        }
        #endregion

        private void QueueSubscriber_OnMessageReceived(Object objectMessage)
        {
            if (objectMessage.GetType() == typeof(DetermineClientsEvent))
            {
                this.addRecive((DetermineClientsEvent)objectMessage);
            }
            else if (objectMessage.GetType() == typeof(ClientEvent))
            {
                // a client online
                // check client is mine
                // check client in list client?
                // update client state if need
                this.listenClientConnect((ClientEvent)objectMessage);
            }
        }

        private void btn_connection_Click(object sender, EventArgs e)
        {
            string username = txf_username.Text;
            string password = txf_password.Text;
            if (username == "" || password == "")
            {
                MessageBox.Show("Không được thiếu tên tài khoản hoặc pasword", "Thông báo");
            }
            else
            {
                ClientConnectionEvent clientEvent = new ClientConnectionEvent();
                ClientConnection clientConnection = new ClientConnection();
                clientConnection.Username = username;
                clientConnection.Password = password;
                clientEvent.ClientConnection = clientConnection;
                clientEvent.IdListenConnection = _clientID;
                clientEvent.StateConnection = true;

                // send username and password to MOM
                _transmitData.sendDataInit(clientEvent);

                // listen from server
                MyQueueSubscriber queueSubscriber = new MyQueueSubscriber(_clientID);
                queueSubscriber.OnMessageReceived += new QMessageReceivedDelegate(QueueSubscriber_OnMessageReceived);

                btn_connection.Enabled = false;
                btn_disconnect.Enabled = true;
                _username = username;
                txf_username.Enabled = false;
                txf_password.Enabled = false;
            }

        }

        #region region execute event

        // add client to recive
        private void addRecive(DetermineClientsEvent determineEvent)
        {
            this._deviceName = determineEvent.ClientName;
            thisToken = determineEvent.ThisToken;

            // connect from device to manager
            // trammit state of this connection
            ClientEvent this_client = new ClientEvent();
            DetermineClient determine_this_client = new DetermineClient();
            determine_this_client.StateConnect = true;
            determine_this_client.SubcriberName = determineEvent.ClientName;
            determine_this_client.TokenName = thisToken;
            this_client.Determine_client = determine_this_client;

            _transmitData.sendData(this_client, thisToken);

            List<DetermineClient> listClient = new List<DetermineClient>();
            listClient = determineEvent.ListDetermineClients;

            DetermineClient determine = new DetermineClient();
            // add event recive to clients
            for (int index = 0; index < listClient.Count; index++)
            {
                determine = listClient[index];
                createViewClientContact(determine.SubcriberName, determine.StateConnect);
                MyQueueSubscriber queueSubscriber = new MyQueueSubscriber(determine.TokenName);
                queueSubscriber.OnMessageReceived += new QMessageReceivedDelegate(QueueSubscriber_OnMessageReceived);
            }
        }

        #endregion

        #region listening event when a client execute connect or disconnect to server
        private void listenClientConnect(ClientEvent client_event)
        {
            DetermineClient client_connection = new DetermineClient();
            client_connection = client_event.Determine_client;

            this.createViewClientContact(client_connection.SubcriberName, client_connection.StateConnect);


        }

        // create form client connect
        private void createViewClientContact(string client_name, bool state)
        {
            int left_label = 5;
            int width_label = 150;
            int height = 30;

            int width_button = 60;


            Label label = new Label();
            label.Left = left_label;
            label.Top = _distance_to_top_result_form;
            label.Width = width_label;
            label.Height = height;
            label.Text = client_name;

            Label label2 = new Label();
            label2.Left = width_label + left_label + 5;
            label2.Top = _distance_to_top_result_form;
            label2.Width = width_button;
            label2.Height = height;
            if (state)
            {
                label2.Text = "kết nối";
            }
            else
            {
                label2.Text = "không kết nối";
            }


            if (InvokeRequired)
            {
                // after we've done all the processing, 
                this.Invoke(new MethodInvoker(delegate
                {
                    // load the control with the appropriate data
                    this.pn_client.Controls.Add(label);
                    this.pn_client.Controls.Add(label2);
                    _distance_to_top_result_form += 45;
                }));
                return;
            }
        }

        #endregion

        private void btn_disconnect_Click(object sender, EventArgs e)
        {

            string username = txf_username.Text;
            string password = txf_password.Text;

            btn_connection.Enabled = true;
            btn_disconnect.Enabled = false;
            txf_username.Enabled = true;
            txf_password.Enabled = true;

            //notification to server and other client
            ClientConnectionEvent clientEvent = new ClientConnectionEvent();
            ClientConnection clientConnection = new ClientConnection();
            clientConnection.Username = username;
            clientConnection.Password = password;
            clientEvent.ClientConnection = clientConnection;
            clientEvent.IdListenConnection = _clientID;
            clientEvent.StateConnection = false;

            // send username and password to MOM
            _transmitData.sendDataInit(clientEvent);
        }
    }
}
