﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObjectMessage.ObjectEvent
{
    [Serializable]
    public class Stone
    {
        #region Các thông số của viên đá

        // Đường kính viên đá
        private double diameter;

        // Khối lượng viên đá
        private double mass;

        // Vận tốc ban đầu của viên đá
        private double velocity;

        // Góc ném ban đầu của viên đá
        private double angle;

        // Độ nghiêng của của viên đá so với phương ngang
        private double inclined;

        // Toạ độ của viên đá
        //public Coordinate coor;
        public Coordinate Coordinate { get; set; }


        #endregion

        public Stone()
        {

        }

        public Stone(double x_coor, double y_coor)
        {
            Coordinate = new Coordinate
            {
                X_coor = x_coor,
                Y_coor = y_coor
            };
        }

        public double Diameter
        {
            get
            {
                return diameter;
            }

            set
            {
                diameter = value;
            }
        }

        public double Mass
        {
            get
            {
                return mass;
            }

            set
            {
                mass = value;
            }
        }

        public double Velocity
        {
            get
            {
                return velocity;
            }

            set
            {
                velocity = value;
            }
        }

        public double Angle
        {
            get
            {
                return angle;
            }

            set
            {
                angle = value;
            }
        }

        public double Inclined
        {
            get
            {
                return inclined;
            }

            set
            {
                inclined = value;
            }
        }
    }
}
