﻿using ObjectMessage;
using SkippingStoneService;
using System;
using SkippingStone.Execute;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using SkippingStone;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using Assets.Sources;

public class Server : MonoBehaviour
{
    #region Declare graphic objects
	public RectTransform serverControlPanel;
	public RectTransform showThingInLake;
	public GameObject thingInLakePrefab;
    public GameObject clientPrefab;
    public GameObject prefabText;
	public GameObject stonePrefab;
    //public Sprite[] AnimalImages;
    //public GameObject ContentPanel;
    //public GameObject ListItemPrefab;
    #endregion

    #region Declare Manager
    private string KEY_REGISTER_CLIENT = "InitConnection";

    private int _distance_to_top_result_form = 10;
	public Camera cameraClient1View;
	public Camera cameraClient2View;
	public Camera cameraClient3View;
	public Camera managerView;
	public GameObject clientPanel;
	public GameObject serverPanel;
    private DataAccounts _dataAccount;
    private DeviceMaps _deviceMap;
    SendData _transmitData;
	MyQueueSubscriber queueSubscriber;
	public static Dictionary<GameObject, Skipping> stoneList;
    private Dictionary<int, string> _clientIdentities { get; set; }
    #endregion
    
    // Init server
    void Start()
    {
		try {
			if (!string.IsNullOrEmpty (PlayerPrefs.GetString ("momip"))){
				var abc = PlayerPrefs.GetString ("momip");
			SkippingStoneService.SystemParameters.DOMAIN = PlayerPrefs.GetString ("momip");
			}
		} catch(Exception e){
			Debug.Log ("Lỗi momip");
		}
		if (Config.CurrentDevice == DEVICE_TYPE.MANAGER) {
			stoneList = new Dictionary<GameObject, Skipping> ();
			cameraClient1View.enabled = false;
			cameraClient2View.enabled = false;
			cameraClient3View.enabled = false;
			managerView.enabled = true;
			clientPanel.SetActive (false);
			serverPanel.SetActive (true);
			StoneObject.SetActive (false);
			AddThingInLakeToPanel ();
			//UnityThread.initUnityThread();
			Debug.Log ("Server started");
			_clientIdentities = new Dictionary<int, string> ();
			//PlayerPrefs.
			this._dataAccount = new DataAccounts ();
			this._deviceMap = new DeviceMaps ();
			_transmitData = new SendData ();
			// no pamrameter will listen connection
			queueSubscriber = new MyQueueSubscriber (KEY_REGISTER_CLIENT);
			queueSubscriber.OnMessageReceived += new QMessageReceivedDelegate (QueueSubscriber_OnMessageReceived);
		}

    }
    private void QueueSubscriber_OnMessageReceived(System.Object objectMessage)
    {
        // check a object is type of some class
		if (objectMessage.GetType () == typeof(ClientConnectionEvent)) {
			this.executeConnection ((ClientConnectionEvent)objectMessage);
		} else if (objectMessage.GetType () == typeof(StoneEvent)) {
			UnityMainThreadDispatcher.Instance().Enqueue(this.SkippingStoneHandler ((StoneEvent)objectMessage));
		}

    }



    #region region recive Event from server
    // trasmit: state of this client for all client is listening -> state
    // get all client nearby and send to this client -> map
    private void executeConnection(ClientConnectionEvent clientEvent)
    {
        // get string will public data for connect
            string _clientID = clientEvent.IdListenConnection;

            // check username, password
            ClientConnection client_connection = new ClientConnection();
            client_connection = clientEvent.ClientConnection;

            Device deviceInfor = _dataAccount.getClient(client_connection.Username, client_connection.Password);

            if (deviceInfor == null)
            {
                Console.WriteLine("Không đúng thông tin");
            }
            else
            {
				
                if (clientEvent.StateConnection)
                {
                    // update state online of device
                    if (_dataAccount.updateStateOnline(deviceInfor.Id, true))
                    {
					queueSubscriber = new MyQueueSubscriber (deviceInfor.TokenConnect);
					queueSubscriber.OnMessageReceived += new QMessageReceivedDelegate (QueueSubscriber_OnMessageReceived);

                        
						
                        // if true, add element to client
                        // ID, username, password, name
                        //this.createViewClientContact(deviceInfor.Id, deviceInfor.Name, true);

						UnityMainThreadDispatcher.Instance().Enqueue(AddClientIcon(deviceInfor.Id, deviceInfor.Name));

                        // get client in map and send position in map
                        DeviceMaps maps = new DeviceMaps();
                        List<Device> listDevice = new List<Device>();
                        listDevice = _deviceMap.getDeviceNearBy(deviceInfor.Id);

                        DetermineClientsEvent determine_client = new DetermineClientsEvent();
                        determine_client.ClientName = deviceInfor.Name;
                        determine_client.ThisToken = deviceInfor.TokenConnect;
						determine_client.Role = CheckRole (client_connection.Username);
                        if (listDevice != null)
                        {
                            for (int index = 0; index < listDevice.Count; index++)
                            {
                                DetermineClient client = new DetermineClient();

                                client.StateConnect = listDevice[index].Online;
                                client.TokenName = listDevice[index].TokenConnect;
                                client.SubcriberName = listDevice[index].Name;

                                determine_client.ListDetermineClients.Add(client);
                            }

                            // get key to publish for Client to MOM 
                            _transmitData.sendData(determine_client, _clientID);
                        }
                    }
                }
                else
                {
                    // disconnect
                    // update state online of device
                    if (_dataAccount.updateStateOnline(deviceInfor.Id, false))
                    {
                        // trammit state of this connection
                        ClientEvent this_client = new ClientEvent();
                        DetermineClient determine_this_client = new DetermineClient();
                        determine_this_client.StateConnect = false;
                        determine_this_client.SubcriberName = deviceInfor.Name;
                        determine_this_client.TokenName = deviceInfor.TokenConnect;
                        this_client.Determine_client = determine_this_client;

                        // get key to publish for Client to MOM 
                        string key_connect = deviceInfor.TokenConnect;

                        //this.createViewClientContact(deviceInfor.Id, deviceInfor.Name, false);

                        _transmitData.sendData(this_client, key_connect);
                    }
                }
            }
    }
    #endregion

	public Role CheckRole(string userName){
		if (userName == "client1")
			return Role.CLIENT1;
		if (userName == "client2")
			return Role.CLIENT2;
		if (userName == "client3")
			return Role.CLIENT3;
		return Role.USER;
	}

	List<GameObject> children = new List<GameObject>();
    public IEnumerator AddClientIcon(int id, string name)
    {
        GameObject clientItem = Instantiate(clientPrefab);
        clientItem.transform.SetParent(serverControlPanel, false);
        clientItem.GetComponentInChildren<Text>().text = name;
		clientItem.GetComponentInChildren<Button>().GetComponentInChildren<Text>().text = "Disconnect";
        clientItem.GetComponentInChildren<Button>().onClick.AddListener(delegate { Disconnect(id); });
		children.Add (clientItem);
        yield return null;
    }
	//Dictionary<GameObject, Toggle> thingsList = new Dictionary<GameObject, Toggle> ();
	public void AddThingInLakeToPanel()
	{	
		var thingsList = new List<string>(){"Rock","Box","Wheel", "Small Boat"};
		foreach (var name in thingsList){
			GameObject thing = Instantiate(thingInLakePrefab);
			thing.transform.SetParent(showThingInLake, false);
			thing.GetComponentInChildren<Text>().text = name; 
		}
		//thingsList.Add (thing, thing.GetComponentInChildren<Toggle>());
	}
	public InputField Velocity;
	public InputField Mass;
	public InputField Inclined;
	public InputField Diameter;
	public InputField Angle;
	public Dropdown Position;
	public Skipping stone;
	public GameObject clientObject;
	public GameObject StoneObject;
	private Client _client;
	private bool IsStarted = false;
	public double TimeSkip = 0.01;
	public int CallTime = 0;
	private const double ZDIAMETER = 6;

	public IEnumerator executeSkipping(StoneEvent stoneEvent){
		if (!IsStarted) {
			StoneObject.GetComponent<Rigidbody> ().mass = (float)(stoneEvent.Stone.Mass);
			StoneObject.GetComponent<Rigidbody> ().isKinematic = false;
			StoneObject.GetComponent<Rigidbody> ().position = new Vector3 (
				(float)stoneEvent.Stone.Coordinate.X_coor,
				(float)stoneEvent.Stone.Coordinate.Y_coor,
				(float)stoneEvent.Stone.Coordinate.Z_coor);
			StoneObject.GetComponent<Rigidbody> ().velocity = new Vector3 (3, 3, 3);
		}
		//stone.Stone = new Stone (StoneObject.transform.position.x, StoneObject.transform.position.y, StoneObject.transform.position.z,
		//double.Parse (Diameter.text), double.Parse (Mass.text), double.Parse (Velocity.text), double.Parse (Inclined.text));
		//IsStarted = true;
		//StoneObject.transform.localScale += new Vector3 ((float)stone.Stone.Diameter, (float)stone.Stone.Diameter, (float)ZDIAMETER);
		//Debug.Log (Velocity.text + " ++ " + Mass.text); 
		yield return null;
	}
    public void Disconnect(int id)
    {
        Debug.Log("Disconnect client having id: " + id);
    }

	public IEnumerator SkippingStoneHandler(StoneEvent stoneEvent){
		GameObject newStone = Instantiate(stonePrefab);
		newStone.transform.position = new Vector3 (
			(float)stoneEvent.Stone.Coordinate.X_coor, 
			(float)stoneEvent.Stone.Coordinate.Y_coor, 
			(float)stoneEvent.Stone.Coordinate.Z_coor);
		var newSkipping = new Skipping (
			(stoneEvent.Stone.Diameter), 
			(stoneEvent.Stone.Mass),
			(stoneEvent.Stone.Velocity),
			(stoneEvent.Stone.Angle),
			(stoneEvent.Stone.Inclined));
		//newStone.AddComponent<Rigidbody> ();
		/* var initalVelocity = newSkipping.getVelocityAfterBounce (0);
		newStone.GetComponent<Rigidbody> ().velocity = new Vector3(
			(float)initalVelocity.X_coor, 
			(float)initalVelocity.Y_coor, 
			(float)initalVelocity.Z_coor);
			*/
		newStone.GetComponent<Rigidbody> ().mass = (float)stoneEvent.Stone.Mass;
		stoneList.Add (newStone, newSkipping);
		newStone.GetComponent<Rigidbody> ().isKinematic = false;
		var currentVelocityVector = newSkipping.getVelocityAfterBounce (0);
		newStone.GetComponent<Rigidbody> ().velocity = new Vector3 ((float)currentVelocityVector.X_coor, (float)currentVelocityVector.Y_coor, (float)currentVelocityVector.Z_coor);
		//stone.Stone = new Stone (StoneObject.transform.position.x, StoneObject.transform.position.y, StoneObject.transform.position.z,
		//double.Parse (Diameter.text), double.Parse (Mass.text), double.Parse (Velocity.text), double.Parse (Inclined.text));
		//IsStarted = true;
		//StoneObject.transform.localScale += new Vector3 ((float)stone.Stone.Diameter, (float)stone.Stone.Diameter, (float)ZDIAMETER);
		//Debug.Log (Velocity.text + " ++ " + Mass.text); 
		yield return null;
	}

    void Update()
    {
    }
}


