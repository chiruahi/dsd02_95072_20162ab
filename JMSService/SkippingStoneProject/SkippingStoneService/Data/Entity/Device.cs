﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SkippingStoneService.Data.Entity
{
    public class Device
    {
        private int id;
        private string username;
        private string password;
        private string name;

        private string tokenConnect;
        private bool online;

        public Device()
        {
        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Username
        {
            get
            {
                return username;
            }

            set
            {
                username = value;
            }
        }

        public string Password
        {
            get
            {
                return password;
            }

            set
            {
                password = value;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public bool Online
        {
            get
            {
                return online;
            }

            set
            {
                online = value;
            }
        }

        public string TokenConnect
        {
            get
            {
                return tokenConnect;
            }

            set
            {
                tokenConnect = value;
            }
        }
    }
}
