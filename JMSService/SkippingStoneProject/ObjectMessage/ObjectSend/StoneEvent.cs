﻿using ObjectMessage.ObjectEvent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObjectMessage.ObjectSend
{
    [Serializable]
    public class StoneEvent
    {
        private Stone _stone;

        public StoneEvent()
        {

        }

        public Stone Stone
        {
            get
            {
                return _stone;
            }

            set
            {
                _stone = value;
            }
        }
    }
}
