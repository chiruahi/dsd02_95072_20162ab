﻿using Apache.NMS;
using Apache.NMS.ActiveMQ;
using Apache.NMS.Util;
using ObjectMessage.Config;
using ObjectMessage.ObjectSend;
using SkippingStoneService.Connection;
using SkippingStoneService.Data;
using SkippingStoneService.Data.Entity;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ManagerApp
{
    public partial class ClientManager : Form
    {
        private string KEY_REGISTER_CLIENT = "InitConnection";

        private int _distance_to_top_result_form = 10;

        private DataAccounts _dataAccount;
        private DeviceMaps _deviceMap;

        SendData _transmitData;
        MyQueueSubscriber queueSubscriber;

        // save id and token client when init connection


        public ClientManager()
        {
            InitializeComponent();

            this._dataAccount = new DataAccounts();
            this._deviceMap = new DeviceMaps();
            _transmitData = new SendData();

            // no pamrameter will listen connection
            queueSubscriber = new MyQueueSubscriber(KEY_REGISTER_CLIENT);
            queueSubscriber.OnMessageReceived += new QMessageReceivedDelegate(QueueSubscriber_OnMessageReceived);
        }

        private void QueueSubscriber_OnMessageReceived(Object objectMessage)
        {
            // check a object is type of some class
            if (objectMessage.GetType() == typeof(ClientConnectionEvent))
            {
                this.executeConnection((ClientConnectionEvent)objectMessage);
            }
        }


        #region region recive Event from server
        // trasmit: state of this client for all client is listening -> state
        // get all client nearby and send to this client -> map
        private void executeConnection(ClientConnectionEvent clientEvent)
        {
            // get string will public data for connect
            string _clientID = clientEvent.IdListenConnection;

            // check username, password
            ClientConnection client_connection = new ClientConnection();
            client_connection = clientEvent.ClientConnection;

            Device deviceInfor = _dataAccount.getClient(client_connection.Username, client_connection.Password);

            if (deviceInfor == null)
            {
                Console.WriteLine("Không đúng thông tin");
            }
            else
            {
                if (clientEvent.StateConnection)
                {
                    // update state online of device
                    if (_dataAccount.updateStateOnline(deviceInfor.Id, true))
                    {
                        // if true, add element to client
                        // ID, username, password, name
                        this.createViewClientContact(deviceInfor.Id, deviceInfor.Name, true);

                        // get client in map and send position in map
                        DeviceMaps maps = new DeviceMaps();
                        List<Device> listDevice = new List<Device>();
                        listDevice = _deviceMap.getDeviceNearBy(deviceInfor.Id);

                        DetermineClientsEvent determine_client = new DetermineClientsEvent();
                        determine_client.ClientName = deviceInfor.Name;
                        determine_client.ThisToken = deviceInfor.TokenConnect;

                        if (listDevice != null)
                        {
                            for (int index = 0; index < listDevice.Count; index++)
                            {
                                DetermineClient client = new DetermineClient();

                                client.StateConnect = listDevice[index].Online;
                                client.TokenName = listDevice[index].TokenConnect;
                                client.SubcriberName = listDevice[index].Name;

                                determine_client.ListDetermineClients.Add(client);
                            }

                            // get key to publish for Client to MOM 
                            _transmitData.sendData(determine_client, _clientID);
                        }
                    }
                }
                else
                {
                    // disconnect
                    // update state online of device
                    if (_dataAccount.updateStateOnline(deviceInfor.Id, false))
                    {
                        // trammit state of this connection
                        ClientEvent this_client = new ClientEvent();
                        DetermineClient determine_this_client = new DetermineClient();
                        determine_this_client.StateConnect = false;
                        determine_this_client.SubcriberName = deviceInfor.Name;
                        determine_this_client.TokenName = deviceInfor.TokenConnect;
                        this_client.Determine_client = determine_this_client;

                        // get key to publish for Client to MOM 
                        string key_connect = deviceInfor.TokenConnect;

                        this.createViewClientContact(deviceInfor.Id, deviceInfor.Name, false);

                        _transmitData.sendData(this_client, key_connect);
                    }
                }
            }
        }

        // create form client connect
        private void createViewClientContact(int id, string client_name, bool state)
        {
            int left_label = 5;
            int width_label = 150;
            int height = 30;

            int width_button = 60;


            Label label = new Label();
            label.Left = left_label;
            label.Top = _distance_to_top_result_form;
            label.Width = width_label;
            label.Height = height;
            label.Text = client_name + "TT: " + state;

            Button button = new Button();
            button.Left = width_label + left_label + 5;
            button.Top = _distance_to_top_result_form;
            button.Width = width_button;
            button.Height = height;
            button.Text = "Hủy";
            button.Tag = id;
            button.Click += new EventHandler(exitConnection);

            if (InvokeRequired)
            {
                // after we've done all the processing, 
                this.Invoke(new MethodInvoker(delegate
                {
                    // load the control with the appropriate data
                    this.pn_client_connect.Controls.Add(label);
                    this.pn_client_connect.Controls.Add(button);
                    _distance_to_top_result_form += 45;
                }));
                return;
            }
        }

        #endregion

        // close listen from a client in server
        private void exitConnection(object sender, EventArgs e)
        {
            var btn = sender as Button;
            int id_client = (int)btn.Tag;
            // do something


        }
    }
}
