﻿namespace ClientApp
{
    partial class Client
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_connection = new System.Windows.Forms.Button();
            this.txf_username = new System.Windows.Forms.TextBox();
            this.txf_password = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pn_client = new System.Windows.Forms.Panel();
            this.btn_disconnect = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_connection
            // 
            this.btn_connection.Location = new System.Drawing.Point(466, 25);
            this.btn_connection.Name = "btn_connection";
            this.btn_connection.Size = new System.Drawing.Size(75, 23);
            this.btn_connection.TabIndex = 0;
            this.btn_connection.Text = "Kết nối";
            this.btn_connection.UseVisualStyleBackColor = true;
            this.btn_connection.Click += new System.EventHandler(this.btn_connection_Click);
            // 
            // txf_username
            // 
            this.txf_username.Location = new System.Drawing.Point(116, 25);
            this.txf_username.Name = "txf_username";
            this.txf_username.Size = new System.Drawing.Size(302, 20);
            this.txf_username.TabIndex = 1;
            // 
            // txf_password
            // 
            this.txf_password.Location = new System.Drawing.Point(116, 60);
            this.txf_password.Name = "txf_password";
            this.txf_password.Size = new System.Drawing.Size(302, 20);
            this.txf_password.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Username";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Password";
            // 
            // pn_client
            // 
            this.pn_client.Location = new System.Drawing.Point(1, 100);
            this.pn_client.Name = "pn_client";
            this.pn_client.Size = new System.Drawing.Size(553, 333);
            this.pn_client.TabIndex = 5;
            // 
            // btn_disconnect
            // 
            this.btn_disconnect.Location = new System.Drawing.Point(466, 56);
            this.btn_disconnect.Name = "btn_disconnect";
            this.btn_disconnect.Size = new System.Drawing.Size(75, 23);
            this.btn_disconnect.TabIndex = 6;
            this.btn_disconnect.Text = "Hủy kết nối";
            this.btn_disconnect.UseVisualStyleBackColor = true;
            this.btn_disconnect.Click += new System.EventHandler(this.btn_disconnect_Click);
            // 
            // Client
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(553, 431);
            this.Controls.Add(this.btn_disconnect);
            this.Controls.Add(this.pn_client);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txf_password);
            this.Controls.Add(this.txf_username);
            this.Controls.Add(this.btn_connection);
            this.Name = "Client";
            this.Text = "Ao cá nhỏ";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_connection;
        private System.Windows.Forms.TextBox txf_username;
        private System.Windows.Forms.TextBox txf_password;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel pn_client;
        private System.Windows.Forms.Button btn_disconnect;
    }
}

