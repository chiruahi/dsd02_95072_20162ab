﻿namespace SkippingStone.Element
{
    public class Stone
    {
        #region Các thông số của viên đá

        // Đường kính viên đá
        private double diameter;

        // Khối lượng viên đá
        private double mass;

        // Vận tốc ban đầu của viên đá
        private double velocity;

        // Góc ném ban đầu của viên đá
        private double angle;

        // Góc lệch gamma so với mặt phẳng Oxy
        private double inclined;

        // Toạ độ của viên đá
        //public Coordinate coor;
        public Coordinate Coordinate { get; set; }

        #endregion


        public Stone()
        {

        }

        public Stone(double x_coor, double y_coor, double z_coor)
        {
            Coordinate = new Coordinate
            {
                X_coor = x_coor,
                Y_coor = y_coor,
                Z_coor = z_coor
            };
        }

        public Stone(double diameter, double mass, double velocity, double angle, double inclined)
        {
            this.diameter = diameter;
            this.mass = mass;
            this.velocity = velocity;
            this.angle = angle;
            this.inclined = inclined;
        }

        public Stone(double x_coor, double y_coor, double z_coor, double diameter, double mass, double velocity, double angle, double inclined)
        {
            Coordinate = new Coordinate
            {
                X_coor = x_coor,
                Y_coor = y_coor,
                Z_coor = z_coor
            };

            this.diameter = diameter;
            this.mass = mass;
            this.velocity = velocity;
            this.angle = angle;
            this.inclined = inclined;
        }

        public double Diameter
        {
            get
            {
                return diameter;
            }

            set
            {
                diameter = value;
            }
        }

        public double Mass
        {
            get
            {
                return mass;
            }

            set
            {
                mass = value;
            }
        }

        public double Velocity
        {
            get
            {
                return velocity;
            }

            set
            {
                velocity = value;
            }
        }

        public double Angle
        {
            get
            {
                return angle;
            }

            set
            {
                angle = value;
            }
        }

        public double Inclined
        {
            get
            {
                return inclined;
            }

            set
            {
                inclined = value;
            }
        }

    }
}
