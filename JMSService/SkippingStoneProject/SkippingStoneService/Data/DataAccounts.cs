﻿using SkippingStoneService.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SkippingStoneService.Data
{
    public class DataAccounts
    {
        private Database database;
        public DataAccounts()
        {
            database = new Database();
        }


        // open file and check username, password
        // return information: ID, username, password, name
        public Device getClient(string username, string password)
        {
            try
            {
                return database.getAccount(username, password);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }


        /// <summary>
        /// get device by a token
        /// </summary>
        /// <returns></returns>
        public Device getDeviceUsingToken(string token)
        {
            try
            {
                return database.getDeviceUsingToken(token);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }

        /// <summary>
        /// update state of client
        /// </summary>
        /// <param name="deviceID"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public bool updateStateOnline(int deviceID, bool state)
        {
            try
            {
                return database.updateStateOnline(deviceID, state);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
        }


        /// <summary>
        /// get all device online
        /// </summary>
        /// <returns></returns>
        public List<Device> getDeviceOnline()
        {
            try
            {
                return database.getDeviceOnline();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }
    }
}
