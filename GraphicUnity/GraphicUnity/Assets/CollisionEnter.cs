﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SkippingStone.Execute;
using Assets.Sources;
public class CollisionEnter : MonoBehaviour {
	public GameObject stone;

	// Use this for initialization
	void Start () {
		CountTimesControl = new Dictionary<Collider, int> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	private Dictionary<Collider, int> CountTimesControl;
	private float Velocity = 10;
	public void OnTriggerEnter(Collider col){

		//Debug.Log ("Trigger ");
		//col.gameObject.GetComponent<Rigidbody> ().velocity = new Vector3 (4, 4, 4);
		Dictionary<GameObject, Skipping> stoneList = null;
		if (Config.CurrentDevice == DEVICE_TYPE.CLIENT)
			stoneList = Client.stoneList;
		else 
			if (Config.CurrentDevice == DEVICE_TYPE.MANAGER)
				stoneList = Server.stoneList;
		if (col.gameObject.GetType () == stone.GetType ()){
			if (CountTimesControl.ContainsKey (col) && stoneList != null && stoneList.ContainsKey(col.gameObject)) {
					var currentVelocityVector = stoneList [col.gameObject].getVelocityAfterBounce (CountTimesControl [col]);
				col.gameObject.GetComponent<Rigidbody> ().velocity = new Vector3 (
					(float)currentVelocityVector.X_coor, (float)currentVelocityVector.Y_coor, (float)currentVelocityVector.Z_coor);
				CountTimesControl [col] = CountTimesControl [col] + 1;
			} else{
				if (CountTimesControl.ContainsKey (col))
					return;
				CountTimesControl.Add(col, 0);
				if (stoneList != null && stoneList.ContainsKey(col.gameObject)){
					var currentVelocityVector = stoneList [col.gameObject].getVelocityAfterBounce (CountTimesControl [col]);
				col.gameObject.GetComponent<Rigidbody> ().velocity = new Vector3 ((float)currentVelocityVector.X_coor,
					(float)currentVelocityVector.Y_coor, (float)currentVelocityVector.Z_coor);
					CountTimesControl [col] = CountTimesControl [col] + 1;
				}
			}
		}
	
}
}
