﻿using Apache.NMS;
using Apache.NMS.ActiveMQ;
using Apache.NMS.Util;
using SkippingStoneService.Config;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SkippingStoneService.Connection
{
    public delegate void QMessageReceivedDelegate(Object objectMessage);
    public class MyQueueSubscriber : IDisposable
    {
        private readonly string topicName = null;
        private readonly IConnectionFactory connectionFactory;
        private readonly IConnection connection;
        private readonly ISession session;
        private readonly IDestination destination;
        private readonly IMessageConsumer consumer;
        private bool isDisposed = false;
        public event QMessageReceivedDelegate OnMessageReceived;

        public MyQueueSubscriber(string keyRecive)
        {
            string brokerUri = SystemParameters.DOMAIN;
            //this.topicName = null;
            this.connectionFactory = new ConnectionFactory(brokerUri);
            this.connection = this.connectionFactory.CreateConnection();
            //this.connection.ClientId = clientId;
            this.connection.Start();
            this.session = connection.CreateSession(AcknowledgementMode.AutoAcknowledge);
            this.destination = SessionUtil.GetDestination(session, keyRecive);

            this.consumer = this.session.CreateConsumer(destination);
            this.consumer.Listener += new MessageListener(OnMessage);

        }

        public void OnMessage(IMessage message)
        {
            IObjectMessage objMessage = message as IObjectMessage;
            Object objectMessage = new Object();
            objectMessage = objMessage.Body;
            if (this.OnMessageReceived != null)
            {
                this.OnMessageReceived(objectMessage);
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (!this.isDisposed)
            {
                this.consumer.Dispose();
                this.session.Dispose();
                this.connection.Dispose();
                this.isDisposed = true;
            }
        }

        #endregion
    
    }
}
