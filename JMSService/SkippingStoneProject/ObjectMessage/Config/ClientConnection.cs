﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObjectMessage.Config
{
    [Serializable]
    public class ClientConnection
    {
        // connect to server using username and password
        private string _username;
        private string _password;

        public string Username
        {
            get
            {
                return _username;
            }

            set
            {
                _username = value;
            }
        }

        public string Password
        {
            get
            {
                return _password;
            }

            set
            {
                _password = value;
            }
        }
    }
}
