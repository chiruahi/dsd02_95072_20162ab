﻿namespace SkippingStone.Element
{
    public class Coordinate
    {
        private double x_coor;
        private double y_coor; // y_coor là toạ độ theo phương thẳng đứng
        private double z_coor;

        public Coordinate()
        {

        }

        public Coordinate(double x_coor, double y_coor, double z_coor)
        {
            this.x_coor = x_coor;
            this.y_coor = y_coor;
            this.z_coor = z_coor;
        }

        public double X_coor
        {
            get
            {
                return x_coor;
            }

            set
            {
                x_coor = value;
            }
        }

        public double Y_coor
        {
            get
            {
                return y_coor;
            }

            set
            {
                y_coor = value;
            }
        }

        public double Z_coor
        {
            get
            {
                return z_coor;
            }

            set
            {
                z_coor = value;
            }
        }
    }
}
