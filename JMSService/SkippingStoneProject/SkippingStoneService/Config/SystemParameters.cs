﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SkippingStoneService.Config
{
    public class SystemParameters
    {
        // domain
        public static string DOMAIN = "tcp://localhost:61616";

        // key to listen response from MOM when start connection
        public static string KEY_LISTEN_CONNECT_FROM_SERVER = "DevicesConnection";

        // key put information register connect to server
        public static string KEY_REGISTER_CLIENT = "InitConnection";
    }
}
