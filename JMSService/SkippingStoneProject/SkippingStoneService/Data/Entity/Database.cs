﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SkippingStoneService.Data.Entity
{
    public class Database
    {
        /// <summary>
        /// List device 
        /// </summary>
        private List<Device> listDevice = new List<Device>()
        {
            new Device { Id = 1, Username = "client1", Password = "e10adc3949ba59abbe56e057f20f883e", Name = "Client 01", TokenConnect = "94a08da1fecbb6e8b46990538c7b50b2",Online = false },
            new Device { Id = 2, Username = "client2",Password = "e10adc3949ba59abbe56e057f20f883e", Name = "Client 02", TokenConnect = "78b1e6d775cec5260001af137a79dbd5",Online = false },
            new Device { Id = 3, Username = "client3",Password = "e10adc3949ba59abbe56e057f20f883e", Name = "Client 03", TokenConnect = "0e0530c1430da76495955eb06eb99d95", Online = false}
        };

        /// <summary>
        ///  Init of area
        /// </summary>
        /// <returns></returns>
        private List<int> mapDevice = new List<int>()
        {
            { 2 }, { 3 }, { 1 }
        };


        /// <summary>
        /// Get account if username and password pass
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>

        public Device getAccount(string username, string password)
        {
            try
            {
                foreach (Device device in listDevice)
                {
                    if (username == device.Username)
                    {
                        if (String.Compare(FormatParameters.EncodeStringToMD5(password), device.Password, true) == 0)
                        {
                            return device;
                        }
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }

        /// <summary>
        /// get list device and state near by a device
        /// </summary>
        /// <param name="deviceID"></param>
        /// <returns></returns>
        public List<Device> getDeviceNear(int deviceID)
        {
            try
            {
                List<Device> list = new List<Device>();
                int count = mapDevice.Count;
                for (int index = 0; index < count; index++)
                {
                    if (mapDevice[index] != deviceID)
                    {
                        list.Add(getDevice(mapDevice[index]));
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }

        /// <summary>
        /// get device by ID
        /// </summary>
        /// <param name="deviceID"></param>
        /// <returns></returns>
        private Device getDevice(int deviceID)
        {
            try
            {
                foreach (Device device in listDevice)
                {
                    if (device.Id == deviceID)
                    {
                        return device;
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }

        /// <summary>
        /// update state online of device
        /// </summary>
        /// <param name="deviceID"></param>
        /// <returns></returns>
        public bool updateStateOnline(int deviceID, bool state)
        {
            try
            {
                for (int index = 0; index < listDevice.Count; index++)
                {
                    if (listDevice[index].Id == deviceID)
                    {
                        listDevice[index].Online = state;
                        return true;
                    }
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
        }

        /// <summary>
        /// get Device where state online is true
        /// </summary>
        /// <returns></returns>
        public List<Device> getDeviceOnline()
        {
            try
            {
                List<Device> list = new List<Device>();
                foreach (Device device in listDevice)
                {
                    if (device.Online)
                    {
                        list.Add(device);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }

        /// <summary>
        /// get device by a token
        /// </summary>
        /// <returns></returns>
        public Device getDeviceUsingToken(string token)
        {
            try
            {
                foreach (Device device in listDevice)
                {
                    if (device.TokenConnect == token)
                    {
                        return device;
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }

        /// <summary>
        /// add device
        /// </summary>
        /// <returns></returns>
        public Device addDevice(string username, string passord, string name)
        {
            try
            {
                int newID = listDevice[listDevice.Count - 1].Id + 1;

                // add new device
                Device newDevice = new Device
                {
                    Id = newID,
                    Username = username,
                    Password = FormatParameters.EncodeStringToMD5(passord),
                    Name = name,
                    TokenConnect = FormatParameters.getRamdomToken(),
                    Online = false
                };

                listDevice.Add(newDevice);

                // add in construct map
                mapDevice.Add(newID);

                return newDevice;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }

        /// <summary>
        /// delete a device in list
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool disposedevice(string username, string password)
        {
            try
            {
                Device device = new Device();
                device = this.getAccount(username, password);

                // remove a element in array
                return listDevice.Remove(device);
            }
            catch (exception ex)
            {
                console.writeline(ex);
                return false;
            }
        }



    }
}
