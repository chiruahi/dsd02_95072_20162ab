﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SkippingStone;
using SkippingStone.Execute;
using UnityEngine.UI;
using ObjectMessage.ObjectSend;

public class SkippingAction : MonoBehaviour {
	public InputField Velocity;
	public InputField Mass;
	public InputField Inclined;
	public InputField Diameter;
	public InputField Angle;
	public Dropdown Position;
	public Skipping stone;
	public GameObject clientObject;
	public GameObject StoneObject;
	private Client _client;
	private bool IsStarted = false;
	public double TimeSkip = 0.01;
	public int CallTime = 0;
	private const double ZDIAMETER = 6;
	// Use this for initialization
	void Start () {
		
	}

	public void SkippingOnClick(){
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
