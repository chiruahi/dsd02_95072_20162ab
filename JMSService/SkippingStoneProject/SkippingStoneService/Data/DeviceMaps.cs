﻿using SkippingStoneService.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SkippingStoneService.Data
{
    public class DeviceMaps
    {
        private Database database;
        public DeviceMaps()
        {
            database = new Database();
        }

        /// <summary>
        /// get device near by using ID
        /// </summary>
        /// <param name="deviceID"></param>
        /// <returns></returns>
        public List<Device> getDeviceNearBy(int deviceID)
        {
            try
            {
                return database.getDeviceNear(deviceID);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }
    }
}
