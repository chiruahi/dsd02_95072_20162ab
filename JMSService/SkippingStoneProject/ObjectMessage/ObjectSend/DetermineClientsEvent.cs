﻿using ObjectMessage.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObjectMessage.ObjectSend
{
    [Serializable]
    public class DetermineClientsEvent
    {
        // this client
        private string _clientName;
        private string _thisToken;

        private List<DetermineClient> _listDetermineClients;

        public DetermineClientsEvent()
        {
            ListDetermineClients = new List<DetermineClient>();
            _clientName = "";
        }

        public string ClientName
        {
            get
            {
                return _clientName;
            }

            set
            {
                _clientName = value;
            }
        }

        public List<DetermineClient> ListDetermineClients
        {
            get
            {
                return _listDetermineClients;
            }

            set
            {
                _listDetermineClients = value;
            }
        }

        public string ThisToken
        {
            get
            {
                return _thisToken;
            }

            set
            {
                _thisToken = value;
            }
        }
    }
}
