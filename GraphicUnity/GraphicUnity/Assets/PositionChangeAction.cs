﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PositionChangeAction : MonoBehaviour {
	public Dropdown PositionDropdown;
	public GameObject Position1;
	public GameObject Position2;
	public GameObject Position3;
	public GameObject Stone;
	// Use this for initialization
	void Start () {
		//Stone.transform.position = Position1.transform.position;
	}

	public void ValueOnChanged(int index){
		switch(index)
		{
		case 0:
			Stone.GetComponent<Rigidbody> ().isKinematic = true;
			Stone.GetComponent<Rigidbody> ().useGravity = false;
			Stone.transform.position = Position1.transform.position;
			break;
		case 1:
			Stone.GetComponent<Rigidbody> ().isKinematic = true;
			Stone.GetComponent<Rigidbody> ().useGravity = false;
			Stone.transform.position = Position2.transform.position;
			break;
		case 2:
			Stone.GetComponent<Rigidbody> ().isKinematic = true;
			Stone.GetComponent<Rigidbody> ().useGravity = false;
			Stone.transform.position = Position3.transform.position;
			break;
		default:
			break;
		}
		
	}

	// Update is called once per frame
	void Update () {
		
	}
}
